package main

import (
	"fmt"
	"sort"
)

func ReturnInt() int {
	var i int = 1
	return i
}

func ReturnFloat() float32 {
	var i float32 = 1.1
	return i
}

func ReturnIntArray() [3]int {
	i := [3]int{1, 3, 4}
	return i
}

func ReturnIntSlice() []int {
	i := []int{1, 2, 3}
	return i
}

func IntSliceToString(slice []int) string {
	var str string
	for _, val := range slice {
		str = str + fmt.Sprintf("%d", val)
	}
	return str
}

func MergeSlices(sl1 []float32, sl2 []int32) []int {
	var i []int
	for _, val := range sl1 {
		i = append(i, int(val))
	}
	for _, val := range sl2 {
		i = append(i, int(val))
	}
	return i
}

func GetMapValuesSortedByKey(InputMap map[int]string) []string {
	var i []string
	var Keys []int
	for k := range InputMap {
		Keys = append(Keys, k)
	}
	sort.Ints(Keys)
	for _, k := range Keys {
		i = append(i, InputMap[k])
	}
	return i
}

func main() {
	fmt.Println(IntSliceToString([]int{17, 23, 100500}))
}